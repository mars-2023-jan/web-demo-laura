package com.training.web;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.util.Objects;

@NoArgsConstructor
@AllArgsConstructor
@Data

public class Logon {
	
	private int userID;
	private String userName;
	private String userPwd;
	
	
}
