package com.training.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// Import Database Connection Class file
import com.training.web.LoginJDBC;

// Servlet Name
@WebServlet("/Login")

public class LoginServelet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			String url = "jdbc:mysql://localhost:3306/Java";
			String user = "root";
			String password = "Emk1Lfk2Crk3!";
			Connection con = DriverManager.getConnection(url, user, password);
	
			// get name and password entered
			String userName = request.getParameter("uname");
			String userPwd = request.getParameter("pwd");
					
			//  query database for user name entered
			PreparedStatement pstmt = con.prepareStatement("select * from users where userName = ?");
			pstmt.setString(1, userName);
			
			// return result set
			ResultSet rs = pstmt.executeQuery();
			
			// initialize dispatcher
			RequestDispatcher rd = null;
			
			if (rs.next() == false) {
				// there are no records matching that name
				
				// get all records from database
				List<Logon> logonList = new ArrayList<Logon>();
				PreparedStatement pstmt2 = con.prepareStatement("select * from users");
				ResultSet rs2 = pstmt2.executeQuery();
				while(rs2.next()) {
					Logon log = new Logon();
					log.setUserID(rs2.getInt(1));
					log.setUserName(rs2.getString(2));
					log.setUserPwd(rs2.getString(3));
					logonList.add(log);
				};
				//save the user name & list of all available user/pwd 
				request.setAttribute("logList", logonList);		
				request.setAttribute("usrName", userName);
				rd = request.getRequestDispatcher("NoUser.jsp");
				rd.forward(request, response);	
				
			} else {
				do {	
					// user exists, collect password
					String userMatch = rs.getString("userName");
					String pwdMatch = rs.getString("userPwd");
					
					if (userName.equalsIgnoreCase(userMatch) && userPwd.equals(pwdMatch)) {
						//user name & password match
						request.setAttribute("usrName", userMatch);
						rd = request.getRequestDispatcher("Success.jsp");
						rd.forward(request, response);	
					}
					else {
						// user name and password not a match
						rd = request.getRequestDispatcher("Failure.jsp");
						rd.forward(request, response);	
					}
					
				} while (rs.next());
			}
		

		}
		catch (Exception e) {
			e.printStackTrace();
		}

		
		}
	}


