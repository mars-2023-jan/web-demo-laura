<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import = "java.util.List" %>
    <%@ page import = "com.training.web.Logon" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<h1 style="color:red">Cannot Find User ${usrName}</h1>
<h2>Available Users</h2>

<% List<Logon> logList = (List<Logon>)request.getAttribute("logList"); %>
		
<table border = "1">
	<tr>
		<th>User ID</th>
		<th>User Name</th>
		<th>User Password</th>
	</tr>
	

<%
	for(Logon log:logList) {
%>	
	<tr>
		<td><%=log.getUserID() %></td>
		<td><%=log.getUserName() %></td>
		<td><%=log.getUserPwd() %></td>
	</tr>


<%
	};
%>

</table>

</body>
</html>